document.addEventListener('DOMContentLoaded', function() {
    const keys = Array.from(document.querySelectorAll('.key'));

    function removeTransition(e) {
        this.classList.remove('playing');
    }

    function playSound(e) {
        const key = document.querySelector(`.key[data-key="${e.keyCode}"`);
        const audio = document.querySelector(`audio[data-key="${e.keyCode}"`);

        if(!audio) return;
        key.classList.add('playing');
        audio.currentTime = 0;
        audio.play();
    }

    keys.forEach(key => key.addEventListener('transitionend', removeTransition));
    window.addEventListener('keydown', playSound);
})